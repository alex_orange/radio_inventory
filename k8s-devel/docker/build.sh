#!/bin/bash
rsync -av ../../radio_inventory/ app/
rsync -av ../../../special_powers/ app/
docker build -t docker-registry-auth.k8s.flux.utah.edu/radio-inventory-dev .
docker push docker-registry-auth.k8s.flux.utah.edu/radio-inventory-dev
