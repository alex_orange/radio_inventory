#!/bin/bash

set -e

kubectl -n radio-inventory-dev delete deployments --all
./build.sh
kubectl apply -f ../radio-inventory.yaml
watch -n1 kubectl -n radio-inventory-dev get pods
