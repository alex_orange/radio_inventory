#!/bin/bash

POSTGRES_PASSWORD=`openssl rand -base64 32`
DJANGO_SECRET_KEY=`openssl rand -base64 32 | base64`

echo "Generate a ceph authorization."
echo "i.e. ceph auth get-or-create client.radio-inventory mds 'allow rw path=/radio-inventory' mon 'allow r' osd 'allow rw pool=data'"
echo "Assuming username hasn't changed, assuming /radio-inventory is the"
echo "directory to use"
echo "Enter key:"
read CEPH_KEY

cat > config_secrets.yaml <<END
apiVersion: v1
kind: Namespace
metadata:
    name: radio-inventory
    labels:
        app: radio-inventory
---
apiVersion: v1
kind: ConfigMap
metadata:
    name: radio-inventory-config
    namespace: radio-inventory
    labels:
        app: radio-inventory
data:
    database: inventory
    user: django
    django_static_url: https://static.radio-inventory.k8s.flux.utah.edu/
    django_allowed_hosts: radio-inventory.k8s.flux.utah.edu
    django_email_host: 192.168.80.1
    cephfs_user: radio-inventory
    # Spec db host in deployment based off postgres service
    # Spec static url in deployment based off ingress for nginx
---
apiVersion: v1
kind: Secret
metadata:
    name: radio-inventory-secret
    namespace: radio-inventory
    labels:
        app: radio-inventory
type: Opaque
stringData:
    postgres_password: ${POSTGRES_PASSWORD}
data:
    django_secret_key: ${DJANGO_SECRET_KEY}
---
apiVersion: v1
kind: Secret
metadata:
    name: radio-inventory-ceph-secret
    namespace: radio-inventory
    labels:
        app: radio-inventory
type: Opaque
stringData:
    key: ${CEPH_KEY}
END

echo "Don't forget to check the yaml file is the way you want it. Especially"
echo "things like namespace."

echo "********************************************************************************"
echo "Also create directies /radio-inventory/{postgres,backup,upload,static}"
echo "under ceph"
echo "********************************************************************************"

