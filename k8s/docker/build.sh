#!/bin/bash

set -e

rm -R app
mkdir app

pushd ../../radio_inventory
git archive --format=tar -o ../k8s/docker/app.tar HEAD .
popd

pushd app
tar xf ../app.tar
popd
rm app.tar

if [ "x$1" == "xspecial" ]
then
    rsync -av ../../../special_powers/special_powers app/
fi

docker build -t docker-registry-auth.k8s.flux.utah.edu/radio-inventory:0.0.42 .
docker push docker-registry-auth.k8s.flux.utah.edu/radio-inventory:0.0.42
