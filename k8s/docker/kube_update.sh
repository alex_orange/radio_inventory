#!/bin/bash

set -e

vim build.sh ../radio-inventory.yaml
git add build.sh ../radio-inventory.yaml  && git commit
./build.sh && kubectl apply -f ../radio-inventory.yaml

