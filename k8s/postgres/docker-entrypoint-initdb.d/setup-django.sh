#!/bin/bash
psql -U "$POSTGRES_USER" <<-EOSQL
CREATE DATABASE $DJANGO_POSTGRES_DB_NAME;
CREATE USER $DJANGO_POSTGRES_USER WITH PASSWORD '$DJANGO_POSTGRES_PASSWORD';
GRANT ALL PRIVILEGES ON DATABASE $DJANGO_POSTGRES_DB_NAME TO $DJANGO_POSTGRES_USER;
EOSQL
