from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'auth'
    label = 'ldb_auth'
