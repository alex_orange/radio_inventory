from django.urls import path, include
from auth import views

urlpatterns = [
    path('signup', views.SignUpView.as_view(), name='auth__signup'),
    path('activate/<int:pk>/<str:token>', views.ActivateView.as_view(),
         name='auth__activate'),
    path('django/', include('django.contrib.auth.urls')),
]
