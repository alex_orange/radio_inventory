from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.contrib.auth.tokens import default_token_generator
from django.template.response import TemplateResponse
from django.template.loader import render_to_string
from django.conf import settings

from django.views.generic import View
from django.views.generic.edit import FormView

from auth.forms import SignUpForm

UserModel = get_user_model()

# Create your views here.
class SignUpView(FormView):
    form_class = SignUpForm
    template_name = "form.html"
    success_url = "bogus"

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.save()

        mail_subject = "Activate your account"
        context = {'user': user,
                   'base_url': settings.SITE_BASE_URL,
                   'token': default_token_generator.make_token(user)}
        message = render_to_string('auth/activate_email.txt', context)
        to_email = form.cleaned_data.get('email')
        send_mail(mail_subject, message,
                  settings.EMAIL_NO_REPLY, [to_email])

        super().form_valid(form)

        return TemplateResponse(self.request,
                                'auth/activation_see_email.html', {})


class ActivateView(View):
    # TODO: Add resend activation email feature for invalid links
    def get(self, request, pk=None, token=None, *args, **kwargs):
        try:
            user = UserModel._default_manager.get(pk=pk)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and default_token_generator.check_token(user,
                                                                    token):
            user.is_active = True
            user.save()
            return TemplateResponse(request, 'auth/activate_success.html', {})
        else:
            return TemplateResponse(request, 'auth/activate_failure.html', {})
