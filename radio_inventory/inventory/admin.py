from django.contrib import admin

from inventory.models import (Location, NodeType, Role, NodePortClass,
                              NodePort, Device, DeviceConnection)

# Register your models here.
admin.site.register(Location)
admin.site.register(NodeType)
admin.site.register(Role)
admin.site.register(NodePortClass)
admin.site.register(NodePort)
admin.site.register(Device)
admin.site.register(DeviceConnection)
