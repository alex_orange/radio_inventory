from django.urls import path

from inventory import views

urlpatterns = [
    path('node_port_class_compatibility_list/',
         views.NodePortClassJsonListView.as_view(),
         name='api__inventory__node_port_class__compatibility_list'),
]
