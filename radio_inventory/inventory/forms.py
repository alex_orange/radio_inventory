from django.forms import ModelForm
from django import forms
from django.forms.widgets import Select, TextInput


from inventory.models import (Location, NodeType, NodeTypeImage, Role,
                              NodePortClass, NodePort, Device,
                              DeviceConnection, LocationNote, LocationImage)
from reference_numbers.forms import ReferenceNumberFormMixin

class LocationCreateForm(ReferenceNumberFormMixin, ModelForm):
    class Meta:
        model = Location
        fields = ['description']


class LocationForm(ModelForm):
    class Meta:
        model = Location
        fields = ['description']


class UploadLocationImageForm(ModelForm):
    class Meta:
        model = LocationImage
        fields = ['location', 'image']


class LocationNoteCreateForm(ModelForm):
    class Meta:
        model = LocationNote
        fields = ['location', 'note']

    def save(self, commit=True, **kwargs):
        super().save(commit=False, **kwargs)
        self.instance.creator = self.creator
        if commit:
            self.instance.save()
            self.save_m2m()
        return self.instance


class LocationNoteForm(ModelForm):
    class Meta:
        model = LocationNote
        fields = ['location', 'note']


class NodeTypeForm(ModelForm):
    class Meta:
        model = NodeType
        fields = ['type_name', 'short_name']


class UploadNodeTypeImageForm(ModelForm):
    class Meta:
        model = NodeTypeImage
        fields = ['node_type', 'image']


class RoleForm(ModelForm):
    class Meta:
        model = Role
        fields = ['description', 'node_type']


class NodePortClassForm(ModelForm):
    class Meta:
        model = NodePortClass
        fields = ['class_name', 'compatible_opposite_ports', 'connection_sink']


class NodePortForm(ModelForm):
    class Meta:
        model = NodePort
        fields = ['node_type', 'port_name', 'port_class']


class PortSelectWidget(Select):
    option_template_name = 'inventory/port_select_option.html'

class LocationSelectWidget(Select):
    option_template_name = 'inventory/device_select_option.html'

class DeviceSelectWidget(Select):
    option_template_name = 'inventory/device_select_option.html'


class DeviceForm(ModelForm):
    class Meta:
        model = Device
        fields = ['emulab_node_name', 'node_type', 'serial_number', 'location',
                  'role']
        widgets = {'location':
                        DeviceSelectWidget(attrs={'id': 'location_select_id'}),
                   'serial_number':
                        TextInput(attrs={'id': 'serial_number_id'}),
                  }


class DeviceCreateForm(ReferenceNumberFormMixin, ModelForm):
    class Meta:
        model = Device
        fields = ['emulab_node_name', 'node_type', 'serial_number', 'location',
                  'role']
        widgets = {'location':
                        DeviceSelectWidget(attrs={'id': 'location_select_id'}),
                   'serial_number':
                        TextInput(attrs={'id': 'serial_number_id'}),
                  }

class DeviceConnectionForm(ModelForm):
    class Meta:
        model = DeviceConnection
        fields = ['a_device', 'b_device', 'a_port', 'b_port']
        widgets = {
            'a_device': DeviceSelectWidget(attrs={'id': 'a_device_select'}),
            'b_device': DeviceSelectWidget(attrs={'id': 'b_device_select'}),
            'a_port': PortSelectWidget(attrs={'id': 'a_port_select'}),
            'b_port': PortSelectWidget(attrs={'id': 'b_port_select'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['a_device'].queryset = \
                Device.objects.prefetch_related('node_type', 'location')
        self.fields['b_device'].queryset = \
                Device.objects.prefetch_related('node_type', 'location')
        self.fields['a_port'].queryset = \
                NodePort.objects.prefetch_related('node_type', 'port_class')
        self.fields['b_port'].queryset = \
                NodePort.objects.prefetch_related('node_type', 'port_class')
