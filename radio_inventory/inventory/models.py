from django.db import models
from django.urls import reverse_lazy
from django.contrib.auth.models import User

from reference_numbers.models import ReferenceNumber
from reference_numbers.mixins import ReferenceNumberMixin


# Create your models here.
class Location(ReferenceNumberMixin, models.Model):
    class Meta:
        ordering = ['description']

    reference_prefix = '1000'
    reference_number = models.OneToOneField(ReferenceNumber,
                                            on_delete=models.PROTECT)
    description = models.CharField(max_length=255)

    def get_absolute_url(self):
        return reverse_lazy('inventory__location-detail',
                            kwargs={'pk': self.pk})

    def __repr__(self):
        return (f"Location(description='{self.description}')")

    def __str__(self):
        return self.description

    def json(self):
        return {"pk": self.pk,
                "reference_number": self.reference_number_str(),
                "description": self.description,
               }

    def graphviz_dot(self):
        title = f"Location: {self.description}"
        node_names = []
        undirected_links = []
        directed_links = []

        for device in self.device_set.all():
            node_names.append((device.pk, device.node_type.short_name))

        # Implicit assumption device connections are within locations (not
        # between)
        connections = DeviceConnection.objects.filter(a_device__location=self)
        for device_connection in connections:
            a_connection_sink = \
                    device_connection.a_port.port_class.connection_sink
            b_connection_sink = \
                    device_connection.b_port.port_class.connection_sink

            if a_connection_sink == True and b_connection_sink == True:
                # TODO: Log this somewhere
                pass

            if ((not a_connection_sink and not b_connection_sink) or
                    (a_connection_sink and b_connection_sink)):
                undirected_links.append((device_connection.a_device.pk,
                                         device_connection.b_device.pk,
                                         device_connection.a_port.port_name,
                                         device_connection.b_port.port_name))

            if a_connection_sink and not b_connection_sink:
                directed_links.append((device_connection.b_device.pk,
                                       device_connection.a_device.pk,
                                       device_connection.b_port.port_name,
                                       device_connection.a_port.port_name))

            if b_connection_sink and not a_connection_sink:
                directed_links.append((device_connection.a_device.pk,
                                       device_connection.b_device.pk,
                                       device_connection.a_port.port_name,
                                       device_connection.b_port.port_name))


        graphviz_result = f"""
digraph {{
    nodesep=1.0;
    {'''
    '''.join([f'{node_name[0]} [ label="{node_name[1]}" ];'
                  for node_name in node_names])}

    subgraph undir {{
        edge [dir=none]
        {'''
        '''.join([f'{link[0]} -> {link[1]} '
                  f'[ taillabel="{link[2]}" headlabel="{link[3]}" '
                  'fontsize=8 color="blue" ]; '
                  for link in undirected_links])}
    }}

    subgraph dir {{
        {'''
        '''.join([f'{link[0]} -> {link[1]} '
                  f'[ taillabel="{link[2]}" headlabel="{link[3]}" '
                  'fontsize=8 color="blue" ]; '
                  for link in directed_links])}
    }}

    labelloc="t";
    label="{title}";
}}
        """

        return graphviz_result


class LocationImage(models.Model):
    location = models.ForeignKey(Location,
                                 on_delete=models.PROTECT)
    image = models.ImageField(width_field='width', height_field='height')
    width = models.PositiveIntegerField()
    height = models.PositiveIntegerField()

    def get_absolute_url(self):
        return reverse_lazy('inventory__location_image-view',
                            kwargs={'pk': self.pk})


class LocationNote(models.Model):
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    note = models.TextField()
    creator = models.ForeignKey(User, on_delete=models.PROTECT)
    creation_time = models.DateTimeField(auto_now_add=True)
    last_modified_time = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return self.location.get_absolute_url()


class NodeType(models.Model):
    class Meta:
        ordering = ['type_name']

    type_name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=255, blank=True)

    def get_absolute_url(self):
        return reverse_lazy('inventory__node_type-detail',
                            kwargs={'pk': self.pk})

    def __repr__(self):
        return f"NodeType(type_name={self.type_name})"

    def __str__(self):
        return self.type_name

    def json(self):
        return {"pk": self.pk,
                "type_name": self.type_name
               }


class NodeTypeImage(models.Model):
    node_type = models.ForeignKey(NodeType,
                                  on_delete=models.PROTECT)
    image = models.ImageField(width_field='width', height_field='height')
    width = models.PositiveIntegerField()
    height = models.PositiveIntegerField()

    def get_absolute_url(self):
        return reverse_lazy('inventory__node_type_image-view',
                            kwargs={'pk': self.pk})


class Role(models.Model):
    class Meta:
        ordering = ['description']

    description = models.CharField(max_length=255)
    # node_type is an indication to the list when assigning a Device's role
    # and is also used to validate the role that was set. Doesn't really need
    # to be exposed to the user as it's not useful after validation.
    node_type = models.ForeignKey(NodeType, on_delete=models.PROTECT)

    def get_absolute_url(self):
        return reverse_lazy('inventory__role-detail',
                            kwargs={'pk': self.pk})

    def __repr__(self):
        return (f"Role(description={self.description}, "
                f"node_type={self.node_type})")

    def __str__(self):
        return self.description

    def json(self):
        return {"pk": self.pk,
                "description": self.description
               }


class NodePortClass(models.Model):
    class Meta:
        ordering = ['class_name']

    class_name = models.CharField(max_length=255)
    compatible_opposite_ports = models.ManyToManyField("NodePortClass",
                                                       symmetrical=True,
                                                       blank=True)
    # If this is a "connection_sink" then connection with a non-sink will form
    # an arrow to this node. Two connection_sink's should not connect to each
    # other. Two connection_sink == false classes connected to each other
    # implies an undirected link.
    # TODO: Some sort of checking on save compatible_opposite_ports to check
    # not being marked compatible with connection_sink == True on both ends.
    connection_sink = models.BooleanField()

    def get_absolute_url(self):
        return reverse_lazy('inventory__node_port_class-detail',
                            kwargs={'pk': self.pk})

    def __repr__(self):
        return f"NodePortClass(class_name={self.class_name})"

    def __str__(self):
        return self.class_name

    def json(self):
        return {"pk": self.pk,
                "class_name": self.class_name,
                "compatible_opposite_ports":
                    [{"pk": other.pk,
                      "class_name": other.class_name,
                     } for other in self.compatible_opposite_ports_set.all()],
               }


class NodePort(models.Model):
    class Meta:
        ordering = ['node_type', 'port_class', 'port_name']

    node_type = models.ForeignKey(NodeType,
                                  on_delete=models.PROTECT)
    port_name = models.CharField(max_length=255)
    port_class = models.ForeignKey(NodePortClass,
                                   on_delete=models.PROTECT)

    def get_absolute_url(self):
        return reverse_lazy('inventory__node_port-detail',
                            kwargs={'pk': self.pk})

    def __repr__(self):
        return (f"NodePort(node_type={self.node_type!r}, "
                f"port_name={self.port_name}, "
                f"port_class={self.port_class!r})")

    def __str__(self):
        return (f"{self.port_name} of {self.node_type}" +
                ("" if self.port_class is None else
                 f" of type {self.port_class}"))

    def json(self):
        return {"pk": self.pk,
                "node_type": self.node_type.json(),
                "port_name": self.port_name,
                "port_class": self.port_class.json(),
               }


# TODO: Check device and any connected devices are in the same location on
# save
# TODO: Changing node_type requires breaking all links (ports need to change)
class Device(ReferenceNumberMixin, models.Model):
    class Meta:
        ordering = ['location', 'node_type', 'emulab_node_name',
                    'serial_number', 'reference_number']
    reference_prefix = '2000'
    reference_number = models.OneToOneField(ReferenceNumber,
                                            on_delete=models.PROTECT)

    emulab_node_name = models.CharField(max_length=255, unique=True, null=True,
                                        blank=True)
    node_type = models.ForeignKey(NodeType,
                                  on_delete=models.PROTECT)
    serial_number = models.CharField(max_length=255, blank=True)

    location = models.ForeignKey(Location,
                                 on_delete=models.PROTECT)

    # TODO: Verify role on save
    # TODO: Make role list from role.node_type == self.node_type
    role = models.ForeignKey(Role,
                             on_delete=models.PROTECT,
                             null=True, blank=True)

    def get_absolute_url(self):
        return reverse_lazy('inventory__device-detail',
                            kwargs={'pk': self.pk})

    def __repr__(self):
        return (f"Device(reference_number={self.reference_number_str()}, "
                f"emulab_node_name={self.emulab_node_name}, "
                f"node_type={self.node_type!r}, "
                f"serial_number={self.serial_number!r}, "
                f"location={self.location!r}, "
                f"role={self.role!r})")

    def __str__(self):
        return (self.emulab_node_name if self.emulab_node_name is not None else
                (f"{self.node_type} S/N {self.serial_number} at "
                 f"{self.location}"
                 if self.serial_number != "" else
                 (f"{self.node_type} at {self.location} reference_number "
                  f"{self.reference_number_str()}")))

    def json(self):
        return {"pk": self.pk,
                "reference_number": self.reference_number_str(),
                "emulab_node_name": self.emulab_node_name,
                "node_type": self.node_type.json(),
                "serial_number": self.serial_number,
                "location": self.location.json(),
                "role": self.role.json(),
               }


# TODO: Validate device connection makes sense on save
# Sense:
#   * According to allowed port classes
#   * In same location
# Valid:
#   * Port node_type matches device node_type
# TODO: Check that a_device and b_device are in the same location. Also
# probably requires checks in Device to not allow movement without severing
# links, or moving all the devices together.
class DeviceConnection(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['a_port', 'a_device'],
                            name='unique_side_a'),
            models.UniqueConstraint(fields=['b_port', 'b_device'],
                            name='unique_side_b'),
        ]
        ordering = ['a_device', 'b_device', 'a_port', 'b_port']

    a_port = models.ForeignKey(NodePort,
                               on_delete=models.PROTECT,
                               related_name="deviceconnection_a_set")
    a_device = models.ForeignKey(Device,
                                 on_delete=models.PROTECT,
                                 related_name="deviceconnection_a_set")

    b_port = models.ForeignKey(NodePort,
                               on_delete=models.PROTECT,
                               related_name="deviceconnection_b_set")
    b_device = models.ForeignKey(Device,
                                 on_delete=models.PROTECT,
                                 related_name="deviceconnection_b_set")

    def __repr__(self):
        return (f"DeviceConnection(a_port={self.a_port!r}, "
                f"a_device={self.a_device!r}, "
                f"b_port={self.b_port!r}, "
                f"b_device={self.b_device!r})")

    def __str__(self):
        return repr(self)

    def get_absolute_url(self):
        return reverse_lazy('inventory__device_connection-detail',
                            kwargs={'pk': self.pk})

    def json(self):
        return {"pk": self.pk,
                "a_port": self.a_port,
                "a_device": self.a_device,
                "b_port": self.b_port,
                "b_device": self.b_device,
               }
