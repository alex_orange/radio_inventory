import 'https://code.jquery.com/jquery-3.5.1.min.js';
const $ = window.$;

// TODO: FIX THIS!!! Don't use an absolute. Figure some way to compile static
// files.
import '/quagga_js/quagga.min.js';
const Quagga = window.Quagga;
//import { Quagga } from '/quagga_js/quagga.min.js';

var currentOnDetected = null;

export function create_device_barcode_reader(div_selector,
                                             select_input_selector,
                                             video_id) {
    var video_element = $("#"+video_id);
    video_element.slideDown();

    Quagga.init({
        inputStream : {
            name: "Live",
            type: "LiveStream",
/*            constraints: {
                width: {min: 640},
                height: {min: 480},
                aspectRatio: {min: 1, max: 100},
                facingMode: "environment"
            },*/
            target: document.querySelector("#"+video_id)
        },
        decoder: {
            readers: ["code_128_reader",
                      "code_39_reader",
                      "code_93_reader"]
        }
    }, function(err) {
        if(err) {
            console.log(err);
            return;
        }
        console.log("Initialization finished. Ready to start");

        Quagga.start();
    });

    if(currentOnDetected === null) {
        Quagga.onDetected(function(data) {
            currentOnDetected(data);
        });
    }

    currentOnDetected = function(data) {
        console.log(data);

        var result_text = data.codeResult.code;
        var select_element = $(select_input_selector);
        var option_elements = $(select_input_selector + " option");

        var option_found = false;

        option_elements.each(function() {
            if(this.dataset.referenceNumber == result_text) {
                option_found = true;
                select_element.val(this.value).change();
            }
        });

        if(option_found) {
            Quagga.stop();
            video_element.slideUp();
            return;
        }

        var options_found = 0;

        option_elements.each(function() {
            $(this).hide();
            if(this.dataset.serialeNumber == result_text) {
                option_found = this;
                $(this).show();
                ++options_found;
            }
        });

        if(options_found == 1) {
            select_element.val(option_found.value).change();
        }

        if(options_found > 0) {
            Quagga.stop();
            video_element.slideUp();
            return;
        }
    };
}
