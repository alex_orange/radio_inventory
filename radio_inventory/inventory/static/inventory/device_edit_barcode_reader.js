import 'https://code.jquery.com/jquery-3.5.1.min.js';
const $ = window.$;

// TODO: FIX THIS!!! Don't use an absolute. Figure some way to compile static
// files.
import '/quagga_js/quagga.min.js';
const Quagga = window.Quagga;
//import { Quagga } from '/quagga_js/quagga.min.js';

export function create_device_edit_barcode_reader(
    video_div_id,
    serial_number_input_selector,
    location_input_selector
) {
    var video_element = $("#"+video_div_id);
    video_element.slideDown();

    Quagga.init({
        inputStream : {
            name: "Live",
            type: "LiveStream",
/*            constraints: {
                width: {min: 640},
                height: {min: 480},
                aspectRatio: {min: 1, max: 100},
                facingMode: "environment"
            },*/
            target: document.querySelector("#"+video_div_id)
        },
        decoder: {
            readers: ["code_128_reader",
                      "code_39_reader",
                      "code_93_reader"]
        }
    }, function(err) {
        if(err) {
            console.log(err);
            return;
        }
        console.log("Initialization finished. Ready to start");

        Quagga.start();
    });

    Quagga.onDetected(function(data) {
        console.log(data);

        var result_text = data.codeResult.code;

        if(result_text.startsWith("1000-")) {
            var select_element = $(location_input_selector);
            var option_elements = $(location_input_selector + " option");

            option_elements.each(function() {
                if(this.dataset.referenceNumber == result_text) {
                    select_element.val(this.value).change();
                    return;
                }
            });
        } else {
            $(serial_number_input_selector).val(result_text).change();
        }
    });
}
