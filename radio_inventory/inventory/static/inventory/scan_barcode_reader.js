import 'https://code.jquery.com/jquery-3.5.1.min.js';
const $ = window.$;

// TODO: FIX THIS!!! Don't use an absolute. Figure some way to compile static
// files.
import '/quagga_js/quagga.min.js';
const Quagga = window.Quagga;
//import { Quagga } from '/quagga_js/quagga.min.js';


export function create_barcode_reader(video_id, base_url) {
    var video_element = $("#"+video_id);
    video_element.slideDown();

    Quagga.init({
        inputStream : {
            name: "Live",
            type: "LiveStream",
/*            constraints: {
                width: {min: 640},
                height: {min: 480},
                aspectRatio: {min: 1, max: 100},
                facingMode: "environment"
            },*/
            target: document.querySelector("#"+video_id)
        },
        decoder: {
            readers: ["code_128_reader",
                      "code_39_reader",
                      "code_93_reader"]
        }
    }, function(err) {
        if(err) {
            console.log(err);
            return;
        }
        console.log("Initialization finished. Ready to start");

        Quagga.start();
    });

    Quagga.onDetected(function(data) {
        console.log(data);

        var result_text = data.codeResult.code;

        var redirect_url = base_url + encodeURIComponent(result_text);

        window.location.href = redirect_url;
    });
}
