from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import (JsonResponse, HttpResponse, FileResponse,
                         HttpResponseRedirect, HttpResponseForbidden,
                         HttpResponseNotFound)
from django.template.response import TemplateResponse
#from django.core.servers.basehttp import FileWrapper
from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.list import MultipleObjectMixin
from django.conf import settings

from wsgiref.util import FileWrapper

import mimetypes
import os
import io
import subprocess
import re
import urllib.parse


from ldb.inventory.barcode import Code128
from ldb.inventory.barcode.writer import ReportlabCanvasWriter
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch



from ldb.django.contrib.auth.mixins import AuthorizedMixin
from ldb.django.contrib.auth.views import (AuthorizedListView,
                                           AuthorizedDetailView,
                                           AuthorizedCreateView,
                                           AuthorizedUpdateView,
                                           AuthorizedDeleteView)
from ldb.django.contrib.auth.permissions import get_permission_full_codename
from ldb.django.view.generic.edit import InitialKwargsMixin

from inventory.models import (Location, NodeType, NodeTypeImage, Role,
                              NodePortClass, NodePort, Device,
                              DeviceConnection, LocationNote, LocationImage)
from inventory.forms import (LocationCreateForm, LocationForm, NodeTypeForm,
                             UploadNodeTypeImageForm, RoleForm,
                             NodePortClassForm, NodePortForm,
                             DeviceCreateForm, DeviceForm,
                             DeviceConnectionForm, LocationNoteForm,
                             LocationNoteCreateForm,
                             UploadLocationImageForm)

# Create your views here.
class LocationViewMixin:
    model = Location

class LocationUpdateViewMixin(LocationViewMixin):
    form_class = LocationForm


class LocationListView(LocationViewMixin, AuthorizedListView): pass
class LocationDetailView(LocationViewMixin, AuthorizedDetailView): pass
class LocationCreateView(LocationViewMixin, AuthorizedCreateView):
    form_class = LocationCreateForm
class LocationUpdateView(LocationUpdateViewMixin, AuthorizedUpdateView): pass
class LocationDeleteView(LocationViewMixin, AuthorizedDeleteView):
    success_url = reverse_lazy('inventory__location-list')

class LocationBarcodeView(LocationViewMixin, AuthorizedMixin,
                          SingleObjectMixin, View):
    def get(self, request, *args, **kwargs):
        response_io = io.BytesIO()
        location = self.get_object()

        c = canvas.Canvas(response_io, pagesize=(283, 176))
        c.translate(0, -1.5*inch)
        writer = ReportlabCanvasWriter(c)
        Code128(location.reference_number_str(), writer=writer).render(
            writer_options={"module_width": 0.6})
        c.translate(0, 1.5*inch)
        c.drawString(20, 156, location.description)
        c.save()
        response_io.seek(0)

        return FileResponse(response_io, content_type="application/pdf")

class LocationGraphView(LocationViewMixin, AuthorizedMixin, SingleObjectMixin,
                        View):
    def get(self, request, *args, **kwargs):
        location = self.get_object()
        dot_string = location.graphviz_dot()
        read_pipe, write_pipe = os.pipe()
        with subprocess.Popen(["/usr/bin/dot", "-Tpdf"],
                              stdin=subprocess.PIPE,
                              stdout=write_pipe) as pipe:
            pipe.stdin.write(dot_string.encode("ascii"))
            pipe.stdin.close()
        os.close(write_pipe)
        return FileResponse(os.fdopen(read_pipe, "rb"),
                            content_type="application/pdf")



class LocationImageViewMixin:
    model = LocationImage

class LocationImageCreateUpdateViewMixin(LocationImageViewMixin):
    form_class = UploadLocationImageForm

class LocationImageView(LocationImageViewMixin, SingleObjectMixin,
                        AuthorizedMixin, View):
    def get(self, request, *args, **kwargs):
        image = self.get_object()
        filename = os.path.join(settings.MEDIA_ROOT, image.image.name)
        mimetype, encoding = mimetypes.guess_type(filename)
        response = HttpResponse(FileWrapper(image.image),
                                content_type=mimetype)
        response['Content-Length'] = os.path.getsize(filename)
        return response

class LocationImageUploadView(LocationImageCreateUpdateViewMixin,
                              AuthorizedCreateView):
    def get_initial(self):
        location = Location.objects.get(pk=self.kwargs["location_pk"])
        return {"location": location}

    def get_success_url(self):
        return self.object.location.get_absolute_url()

class LocationImageDeleteView(LocationImageViewMixin, AuthorizedDeleteView):
    def get(self, request, *args, **kwargs):
        self.object_to_delete = self.get_object()
        self.location_save = self.object_to_delete.location
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object_to_delete = self.get_object()
        self.location_save = self.object_to_delete.location
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return self.location_save.get_absolute_url()


class LocationNoteViewMixin:
    model = LocationNote

class LocationNoteUpdateViewMixin(LocationNoteViewMixin):
    form_class = LocationNoteForm


class LocationNoteListView(LocationNoteViewMixin, AuthorizedListView): pass
class LocationNoteDetailView(LocationNoteViewMixin, AuthorizedDetailView): pass
class LocationNoteCreateView(LocationNoteViewMixin,
                             AuthorizedCreateView):
    form_class = LocationNoteCreateForm

    def get_initial(self):
        initials = super().get_initial()
        initials['location'] = self.kwargs['location_pk']
        return initials

    def form_valid(self, form):
        form.creator = self.request.user
        return super().form_valid(form)


class LocationNoteUpdateView(LocationNoteUpdateViewMixin,
                             AuthorizedUpdateView): pass
class LocationNoteDeleteView(LocationNoteViewMixin,
                             AuthorizedDeleteView):
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.success_url = reverse_lazy('inventory__location-detail',
                                        kwargs={'pk': self.object.pk})
        return super().delete(request, *args, **kwargs)




class NodeTypeViewMixin:
    model = NodeType

class NodeTypeCreateUpdateViewMixin(NodeTypeViewMixin):
    form_class = NodeTypeForm


class NodeTypeListView(NodeTypeViewMixin, AuthorizedListView): pass
class NodeTypeDetailView(NodeTypeViewMixin, AuthorizedDetailView): pass
class NodeTypeCreateView(NodeTypeCreateUpdateViewMixin, AuthorizedCreateView):
    pass
class NodeTypeUpdateView(NodeTypeCreateUpdateViewMixin, AuthorizedUpdateView):
    pass
class NodeTypeDeleteView(NodeTypeViewMixin, AuthorizedDeleteView):
    success_url = reverse_lazy('inventory__node_type-list')



class NodeTypeImageViewMixin:
    model = NodeTypeImage

class NodeTypeImageCreateUpdateViewMixin(NodeTypeImageViewMixin):
    form_class = UploadNodeTypeImageForm

class NodeTypeImageView(NodeTypeImageViewMixin, SingleObjectMixin,
                        AuthorizedMixin, View):
    def get(self, request, *args, **kwargs):
        image = self.get_object()
        filename = os.path.join(settings.MEDIA_ROOT, image.image.name)
        mimetype, encoding = mimetypes.guess_type(filename)
        response = HttpResponse(FileWrapper(image.image),
                                content_type=mimetype)
        response['Content-Length'] = os.path.getsize(filename)
        return response

class NodeTypeImageUploadView(NodeTypeImageCreateUpdateViewMixin,
                              AuthorizedCreateView):
    def get_initial(self):
        node_type = NodeType.objects.get(pk=self.kwargs["node_type_pk"])
        return {"node_type": node_type}

    def get_success_url(self):
        return self.object.node_type.get_absolute_url()

class NodeTypeImageDeleteView(NodeTypeImageViewMixin, AuthorizedDeleteView):
    def get(self, request, *args, **kwargs):
        self.object_to_delete = self.get_object()
        self.node_type_save = self.object_to_delete.node_type
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object_to_delete = self.get_object()
        self.node_type_save = self.object_to_delete.node_type
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return self.node_type_save.get_absolute_url()



class RoleViewMixin:
    model = Role

class RoleCreateUpdateViewMixin(RoleViewMixin):
    form_class = RoleForm


class RoleListView(RoleViewMixin, AuthorizedListView): pass
class RoleDetailView(RoleViewMixin, AuthorizedDetailView): pass
class RoleCreateView(RoleCreateUpdateViewMixin, InitialKwargsMixin,
                     AuthorizedCreateView): pass
class RoleUpdateView(RoleCreateUpdateViewMixin, AuthorizedUpdateView): pass
class RoleDeleteView(RoleViewMixin, AuthorizedDeleteView):
    success_url = reverse_lazy('inventory__role-list')



class NodePortClassViewMixin:
    model = NodePortClass

class NodePortClassCreateUpdateViewMixin(NodePortClassViewMixin):
    form_class = NodePortClassForm


class NodePortClassListView(NodePortClassViewMixin, AuthorizedListView): pass
class NodePortClassDetailView(NodePortClassViewMixin, AuthorizedDetailView):
    pass
class NodePortClassCreateView(NodePortClassCreateUpdateViewMixin,
                              AuthorizedCreateView): pass
class NodePortClassUpdateView(NodePortClassCreateUpdateViewMixin,
                              AuthorizedUpdateView): pass
class NodePortClassDeleteView(NodePortClassViewMixin, AuthorizedDeleteView):
    success_url = reverse_lazy('inventory__node_port_class-list')



class NodePortViewMixin:
    model = NodePort

class NodePortCreateUpdateViewMixin(NodePortViewMixin):
    form_class = NodePortForm


class NodePortListView(NodePortViewMixin, AuthorizedListView): pass
class NodePortDetailView(NodePortViewMixin, AuthorizedDetailView): pass
class NodePortCreateView(NodePortCreateUpdateViewMixin,
                         InitialKwargsMixin, AuthorizedCreateView): pass
class NodePortUpdateView(NodePortCreateUpdateViewMixin,
                         AuthorizedUpdateView): pass
class NodePortDeleteView(NodePortViewMixin, AuthorizedDeleteView):
    success_url = reverse_lazy('inventory__node_port-list')
class NodePortClassJsonListView(NodePortClassViewMixin, AuthorizedMixin,
                                MultipleObjectMixin, View):
    def get(self, request, *args, **kwargs):
        classes = self.get_queryset().all()
        response_json = {}
        for node_port_class in classes:
            compatible_pks = [other_class.pk
                              for other_class in
                              node_port_class.compatible_opposite_ports.all()]

            response_json[str(node_port_class.pk)] = compatible_pks

        return JsonResponse(response_json)



class DeviceViewMixin:
    model = Device

class DeviceUpdateViewMixin(DeviceViewMixin):
    form_class = DeviceForm
    template_name = 'inventory/device_form_scan.html'


class DeviceListView(DeviceViewMixin, AuthorizedListView): pass
class DeviceDetailView(DeviceViewMixin, AuthorizedDetailView): pass
class DeviceCreateView(DeviceViewMixin, InitialKwargsMixin,
                       AuthorizedCreateView):
    form_class = DeviceCreateForm
    template_name = 'inventory/device_form_scan.html'
class DeviceUpdateView(DeviceUpdateViewMixin, AuthorizedUpdateView): pass
class DeviceDeleteView(DeviceViewMixin, AuthorizedDeleteView):
    success_url = reverse_lazy('inventory__device-list')

class DeviceBarcodeView(DeviceViewMixin, AuthorizedMixin,
                        SingleObjectMixin, View):
    def get(self, request, *args, **kwargs):
        response_io = io.BytesIO()
        device = self.get_object()

        c = canvas.Canvas(response_io, pagesize=(283, 176))
        c.translate(0, -1.5*inch)
        writer = ReportlabCanvasWriter(c)
        Code128(device.reference_number_str(), writer=writer).render(
            writer_options={"module_width": 0.6})
        c.translate(0, 1.5*inch)
        c.drawString(20, 156, device.node_type.type_name)
        c.drawString(20, 136, device.serial_number)
        c.drawString(20, 116, (device.emulab_node_name
                               if device.emulab_node_name is not None else ""))
        c.drawString(20, 96, device.location.description)
        c.save()
        response_io.seek(0)

        return FileResponse(response_io, content_type="application/pdf")



class DeviceConnectionViewMixin:
    model = DeviceConnection

class DeviceConnectionCreateUpdateViewMixin(DeviceConnectionViewMixin):
    form_class = DeviceConnectionForm


class DeviceConnectionScannerView(DeviceConnectionCreateUpdateViewMixin,
                                  InitialKwargsMixin,
                                  AuthorizedCreateView):
    template_name = 'inventory/deviceconnection_scan_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['locations'] = Location.objects.all()
        return context



class DeviceConnectionListView(DeviceConnectionViewMixin, AuthorizedListView):
    pass
class DeviceConnectionDetailView(DeviceConnectionViewMixin,
                                 AuthorizedDetailView): pass
class DeviceConnectionCreateView(DeviceConnectionCreateUpdateViewMixin,
                                 AuthorizedCreateView): pass
class DeviceConnectionUpdateView(DeviceConnectionCreateUpdateViewMixin,
                                 AuthorizedUpdateView): pass
class DeviceConnectionDeleteView(DeviceConnectionViewMixin,
                                 AuthorizedDeleteView):
    success_url = reverse_lazy('inventory__device_connection-list')



class ScanDetailRedirectView(View):
    location_re = re.compile(r"1000-(?P<reference_number>\d{10})")
    device_re = re.compile(r"2000-(?P<reference_number>\d{10})")

    location_perm = get_permission_full_codename(Location, "view")
    device_perm = get_permission_full_codename(Location, "view")

    def get(self, request, encoded_barcode=None, *args, **kwargs):
        """
        Security methods:
            * If user asks for a specific item type (by reference number) they
                must have access to the corresponding model or they get bumped
                before further processing. Therefore they can not even tell if
                item exists or not, only can gather the format of reference
                numbers (not sensitive).
            * For processing serial numbers, if user doesn't have access to the
                corresponding model, it's skipped and if it were valid user
                will get 404 instead of 403. This makes sure existence of
                serial number is not leaked if user doesn't have access to
                model.
        """
        barcode = urllib.parse.unquote(encoded_barcode)

        location_match = self.location_re.match(barcode)

        if location_match:
            if not request.user.has_perm(self.location_perm):
                return HttpResponseForbidden()
            else:
                reference_number = \
                        int(location_match.groupdict()["reference_number"])
                location = Location.objects.get(reference_number__pk=
                                                reference_number)
                return HttpResponseRedirect(location.get_absolute_url())


        device_match = self.device_re.match(barcode)

        if device_match:
            if not request.user.has_perm(self.device_perm):
                return HttpResponseForbidden()
            else:
                reference_number = \
                        int(device_match.groupdict()["reference_number"])
                device = Device.objects.get(reference_number__pk=
                                            reference_number)
                return HttpResponseRedirect(device.get_absolute_url())


        if request.user.has_perm(self.device_perm):
            devices = Device.objects.filter(serial_number=barcode).all()
            if len(devices) > 1:
                return TemplateResponse(request,
                                        'inventory/scan_multiple_serials.html',
                                        {})
            elif len(devices) == 1:
                return HttpResponseRedirect(devices[0].get_absolute_url())


        return HttpResponseNotFound()
