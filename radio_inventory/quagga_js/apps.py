from django.apps import AppConfig


class QuaggaJsConfig(AppConfig):
    name = 'quagga_js'
