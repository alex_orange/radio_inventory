from .base import *

def return_true(request):
    print("return_true")
    return True

INSTALLED_APPS.append('debug_toolbar')

MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')

DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": 'radio_inventory.settings.development.return_true'
}

# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DATABASES['default']['DISABLE_SERVER_SIDE_CURSORS'] = True

