# This is used for operations like makemigrations that just don't need the
# database

import os

os.environ.setdefault('DJANGO_SECRET_KEY', 'bogus')
os.environ.setdefault('DJANGO_ALLOWED_HOSTS', 'bogus')
os.environ.setdefault('DJANGO_DB_PASSWORD', 'bogus')
os.environ.setdefault('DJANGO_STATIC_URL', 'bogus/')
os.environ.setdefault('DJANGO_EMAIL_HOST', 'localhost')
os.environ.setdefault('DJANGO_EMAIL_NO_REPLY', 'no-reply@localhost')
os.environ.setdefault('DJANGO_SITE_BASE_URL', 'http://localhost')

from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
