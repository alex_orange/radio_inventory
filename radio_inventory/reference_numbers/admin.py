from django.contrib import admin
from reference_numbers.models import ReferenceNumber

# Register your models here.
admin.site.register(ReferenceNumber)
