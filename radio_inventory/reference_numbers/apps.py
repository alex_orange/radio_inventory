from django.apps import AppConfig


class ReferenceNumbersConfig(AppConfig):
    name = 'reference_numbers'
