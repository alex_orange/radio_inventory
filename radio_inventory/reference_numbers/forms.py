from django.forms import ModelForm
from django import forms
from django.db.models import Max

from reference_numbers.models import ReferenceNumber, versioned_prefixed_reference_number_re, reference_number_re

class ReferenceNumberFormMixin:
    def save(self, commit=True, **kwargs):
        super().save(commit=False, **kwargs)
        self.instance.reference_number = ReferenceNumber.objects.create()
        if commit:
            self.instance.save()
            self.save_m2m()
        return self.instance


class VersionedReferenceNumberFormMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['reference_number_custom'] = forms.CharField(
            required=False)

    def clean_reference_number_custom(self):
        data = self.cleaned_data['reference_number_custom']

        # Empty means generate one
        if data is None or len(data) == 0:
            return

        match = versioned_prefixed_reference_number_re.match(data)
        if match:
            data = match.groupdict()['reference_number']

        if reference_number_re.match(data):
            return int(data)

        raise forms.ValidationError("Invalid reference number")

    def save(self, commit=True, **kwargs):
        super().save(commit=False, **kwargs)

        reference_number = self.cleaned_data['reference_number_custom']
        if reference_number is None or len(reference_number) == 0:
            self.instance.reference_number = ReferenceNumber.objects.create()
            version_number = 0
        else:
            self.instance.reference_number = ReferenceNumber.objects.get(
                pk=reference_number)

            same_reference_number_objects = type(self.instance).objects.filter(
                reference_number=reference_number)

            if same_reference_number_objects.count() != 0:
                version_number = same_reference_number_objects.aggregate(
                    Max('version_number')) + 1
            else:
                version_number = 0

        self.instance.version_number = version_number

        if commit:
            self.instance.save()
            self.save_m2m()
        return self.instance


class VerifyReferenceNumberForm(forms.Form):
    reference_number = forms.CharField()

    def verify_match(self, reference_numbered_object):
        return self.got() == self.expected(reference_numbered_object)

    def expected(self, reference_numbered_object):
        return reference_numbered_object.reference_number_str()

    def got(self):
        return self.cleaned_data['reference_number'].strip()

    def message(self, reference_numbered_object):
        if self.verify_match(reference_numbered_object):
            return "Good :)"
        else:
            return f"BAD BAD BAD BAD!!!!! Got: {self.got()} expected {self.expected(reference_numbered_object)}"
