class ReferenceNumberMixin:
    def reference_number_str(self):
        return f"{self.reference_prefix}-{self.reference_number_id:010d}"

class VersionedReferenceNumberMixin:
    def reference_number_str(self):
        return (f"{self.reference_prefix}-{self.reference_number_id:010d}-"
                f"{self.version_number:05d}")
