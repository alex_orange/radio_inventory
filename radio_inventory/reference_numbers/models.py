import re

from django.db import models

# Create your models here.
versioned_prefixed_reference_number_re = re.compile(
    r"(?P<prefix>\d{4})-(?P<reference_number>\d{10})(-(?P<version>\d{4}))?")
reference_number_re = re.compile(r"\d{4}-\d{10}")

class ReferenceNumber(models.Model):
    pass
