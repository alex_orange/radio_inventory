from django.shortcuts import render
from django.http import HttpResponse
from ldb.django.contrib.auth.mixins import AuthorizedMixin
from django.views.generic.edit import (FormView, SingleObjectMixin,
                                       SingleObjectTemplateResponseMixin)

from reference_numbers.forms import VerifyReferenceNumberForm

# Create your views here.
class VerifyReferenceNumberView(AuthorizedMixin, SingleObjectMixin,
                                SingleObjectTemplateResponseMixin, FormView):
    # Needs to be provided by the extender, this is needed to keep
    # AuthorizedMixin happy
    model = None
    form_class = VerifyReferenceNumberForm
    template_name_suffix = '_form'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        return HttpResponse(form.message(self.get_object()))
